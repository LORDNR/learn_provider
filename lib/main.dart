import 'package:flutter/material.dart';
import 'package:learn_provider_flutter/models/db_model.dart';
import 'package:learn_provider_flutter/provider/db_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) {
            return DataProvider();
          },
        )
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primaryColor: Colors.amber),
        home: MyHomePage(
          title: 'Learn Provider',
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Consumer(
        builder: (context, DataProvider dataProvider, child) {
          return ListView.builder(
            itemCount: dataProvider.dbmodel.length,
            itemBuilder: (context, int index) {
              dbModel data = dataProvider.dbmodel[index];
              return Card(
                elevation: 5,
                margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 30,
                    child: FittedBox(
                      child: Text(data.price.toString()),
                    ),
                  ),
                  title: Text('${data.name} สายพันธุ์ ${data.species}'),
                  subtitle: Text(data.date.toString()),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
