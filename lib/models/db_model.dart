class dbModel {
  String name;
  String species;
  double price;
  DateTime? date;

  dbModel({
    required this.name,
    required this.species,
    required this.price,
    this.date,
  });
}
