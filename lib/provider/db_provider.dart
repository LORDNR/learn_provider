import 'package:flutter/foundation.dart';
import 'package:learn_provider_flutter/models/db_model.dart';

class DataProvider with ChangeNotifier {
  List<dbModel> dbmodel = [
    dbModel(
        name: "นิล", species: 'เปอร์เซีย', price: 5000, date: DateTime.now()),
    dbModel(name: "ทอง", species: 'เมนคูน', price: 3000, date: DateTime.now()),
    dbModel(
        name: "เงิน", species: 'แร็กดอลล์', price: 5500, date: DateTime.now()),
  ];

  List<dbModel> getDbModel() {
    return dbmodel;
  }

  addDbModel(dbModel statement) {
    dbmodel.add(statement);
  }
}
